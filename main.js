document.addEventListener("DOMContentLoaded", function(event) {

	var url = "https://raw.githubusercontent.com/FreeCodeCamp/ProjectReferenceData/master/GDP-data.json";
	var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];

	fetch(url)
		.then(function(response) {			
			return response.json();
		})
		.then(function(data) {

			d3.select(".notes")
				.append("text")
				.text(data.description);

			var gdp = data.data;
			// height and width of svg
			var margin = {top: 10, right: 10, bottom: 50, left: 70};
			var fullWidth = 960;
			var fullHeight = 660;	
			
			// used for ranges of the scales
			var width = fullWidth - margin.right - margin.left;
			var height = fullHeight - margin.top - margin.bottom;

			var boxWidth = Math.ceil(width/gdp.length);

			// Calculate dates
			var minDate = new Date(gdp[0][0]);
			var maxDate = new Date(gdp[(gdp.length - 1)][0]);

			// Scales
			var x = d3.scaleTime()
				.domain([minDate, maxDate])
				.range([0, width]);

			var y = d3.scaleLinear()
				.domain([0, d3.max(gdp, function(d) {
					return d[1];
				})]) // data min and max
				.range([height, 0]); // the pixels to map to, e.g., the width of the diagram

			// Axes
			var xAxis = d3.axisBottom()
				.scale(x)
				.ticks(10, new Date(gdp[0][0]).string);

			var yAxis = d3.axisLeft()
				.scale(y)
				.ticks(10, gdp[0][1].string);

			// define tooltip
			var tooltip = d3.select("body")
				.append("div")
				.attr("class", "tooltip")
				// .style("opacity", 0);

			// set up chart
			var chart = d3.select(".chart")
				.attr("width", fullWidth)
				.attr("height", fullHeight)
				.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.right + ")");

			// adding axes to chart
			chart.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis);

			chart.append("g")
				.attr("class", "y axis")
				.call(yAxis);

			// text label for axes
			chart.append("text")
				.attr("transform", "translate(" + (width/2) + "," + (height + margin.top + 30) +")")
				.text("Years");

			chart.append("text")
				.attr("transform", "rotate(-90)")
				.attr("y", 0 - margin.left)
				.attr("x", 0 - height/2)
				.attr("dy", "1em")
				.style("text-anchor", "middle")
				.text("Gross Domestic Product, USA");		
				
			// add bars to chart
			chart.selectAll(".bar")
					.data(gdp)
				.enter()
					.append("rect")
					.attr("class", "bar")
					// position on x,y axes
					.attr("x", function(d) { return x(new Date(d[0])); })
					.attr("y", function(d) { return y(d[1]) })
					// height and width of data boxes
					.attr("width", boxWidth)
					.attr("height", function(d) { return height - y(d[1]); })
					// tooltips
					.on("mouseover", function(d) {
						tooltip.transition()
							.duration(200)
							.style("opacity", 1);
						tooltip.html("<strong>" + d[1] + " Billion</strong><br />" + months[new Date(d[0]).getMonth()] + " " + new Date(d[0]).getFullYear() )
							.style("left", (d3.event.pageX) - 50 + "px")
							.style("top", (d3.event.pageY) - 60 + "px");
						})
					.on("mouseout", function(d) {
						tooltip.transition()
							.duration(500)
							.style("opacity", 0);
					});


		});
// end document.addEventListener
});